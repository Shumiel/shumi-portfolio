import React from "react";
import { useInView } from 'react-intersection-observer';
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";

const useStyles = makeStyles((theme) => ({
  root1: {
    flexGrow: 1,
    maxWidth: 752,
    paddingBottom: '20px',
  },
  demo: {
    backgroundColor: '#5b6681',
    boxShadow: '0 0 5px 2px #5b6681'
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
  visible: {
    opacity: 1,
    transition: "opacity 0.5s ease-in",
  },
  noVisible: {
    opacity: 0,
    transition: "opacity 0.5s ease-out",
  },
}));

const Additional = () => {
  const { ref, inView } = useInView({
    /* Optional options */
    threshold: 0.25,
  });
  const classes = useStyles();
  const [info] = React.useState([
    {
      name: "Git basics",
    },
    {
      name: "PostgreSQL basics",
    },
    {
      name: "PHP basics",
    },
    {
      name: "PowerShell",
    },
  ]);

  return (
    <div ref={ref} className={inView ? classes.visible : classes.noVisible}>
      <div className={classes.root1}>
      <Grid container spacing={2} >
        <Grid item xs={12} md={12}>
          <Typography variant="h6" className={classes.title}>
            Other skills:
          </Typography>
          <div className={classes.demo}>
            <List>
              {info.map((ele) => (
                <ListItem key={ele.name}>
                  <ListItemAvatar>
                    <Avatar style={{backgroundColor: "#5b6681", color:'black'}}>
                      <ArrowForwardIosIcon/>
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={ele.name} />
                </ListItem>
              ))}
            </List>
          </div>
        </Grid>
      </Grid>
    </div>
    </div>
  );
};

export default Additional