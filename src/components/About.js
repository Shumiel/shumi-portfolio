import { makeStyles, createStyles } from "@material-ui/core/styles";
//Material UI Components - START
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
//Material UI Components - END
import { useInView } from "react-intersection-observer";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      ...theme.typography,
      padding: theme.spacing(1),
      fontSize: theme.spacing(2),
      backgroundColor: "#5b6681",
    },
    aboutInView: {
      position: "relative",
      textAlign: "justify",
      textJustify: "inter-word",
      backgroundColor: "transparent",
      zIndex: "-10",
      transition: "all 0.8 ease",
      opacity: "1",
      paddingTop: '10px'
    },
    aboutOutView: {
      position: "relative",
      textAlign: "justify",
      textJustify: "inter-word",
      backgroundColor: "red",
      zIndex: "-10",
      opacity: "0",
      transition: "all 0.8 ease",
    },
    aboutBg: {
      padding: "30px",
      transition: "opacity 0.8 ease",
      opacity: "1",
    },
    visible: {
      opacity: '1',
      padding: '30px',
      transition: "all 0.5s ease-in-out",
      transform: 'translateY(0px)'
    },
    noVisible: {
      opacity: '0',
      padding: '0px',
      transition: "all 0.5s ease-in-out",
      transform: 'translateY(-100vh)'
    },
  })
);

const About = () => {
  const classes = useStyles();
  const { ref, inView } = useInView({
    /* Optional options */
    threshold: 0.25,
  });

  return (
    <Container ref={ref} className={classes.aboutInView}>
      <Paper
        className={inView ? classes.visible : classes.noVisible}
        style={{ backgroundColor: "#5b6681" }}
      >
        <Typography
          className="about-me"
          variant="h4"
          component="h2"
          gutterBottom
          style={{
            textDecorationLine: "underline",
            textUnderlineOffset: "8px",
            letterSpacing: "5px",
          }}
        >
          About Me
        </Typography>

        <Typography className={classes.root} variant="body1" gutterBottom>
          Hi! My name is Marcin, I am 25 years old and I like to work as a
          programmer, mainly I enjoy web development and from time to time game
          development. I wish to develop my skills further in the direction of
          being a skilled programmer, so in the future, I would be able to work
          in cool places.
        </Typography>

        <Typography className={classes.root} variant="body1" gutterBottom>
          My life as IT guy is carried to this day since I finished technical
          school, it affected many aspects of my life in a positive way, for
          example I finished studies focused around computer science. This is
          why now I desire even more to focus on specific skills to achieve
          something bigger.
        </Typography>
        <Typography className={classes.root} variant="body1" gutterBottom>
          I had oppurtunity to work on many small part time projects and
          features for different people, it's hard to describe for me how much
          experience I could gather as commercial worker but for sure it would
          be fair if I say it was around 3-4 months focused mainly around web
          development.
        </Typography>
      </Paper>
    </Container>
  );
};

export default About;
