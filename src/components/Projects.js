import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

import react from "../img/react.svg";
import godot from "../img/godot.gif";
import phaser from "../img/phaser.gif";
import angular from "../img/AngularShieldLogo.png";
import wordpress from "../img/wordpress-logo.png";

const useStyles = makeStyles({
  root2: {
    maxWidth: "800px",
    marginTop: "30px",
  },
  media: {
    minHeight: 250,
    height: 450,
    backgroundSize: "contain",
    backgroundColor: "#5b6681"
  },
  visible: {
    opacity: 1,
    transition: "opacity 0.5s ease-in",
  },
  noVisible: {
    opacity: 0,
    transition: "opacity 0.5s ease-out",
  }, content:{
    backgroundColor: '#5b6681'
  }
});

export default function Projects() {
  const [projects] = React.useState([
    {
      key: "project1",
      name: "Godot Engine",
      img: godot,
      desc: "Right now aside of my duties in current job I am learning to work with Godot Engine in free time, so I would be able to create commercial game in the future.",
    },
    {
      key: "project2",
      name: "Phaser",
      img: phaser,
      desc: "Thanks to my studies I had oppurtunity to work with JavaScript and Phaser (HTML5 framework focused around Canvas and WebGL) what in the end allowed me to create basic game focused around Front-End technologies.",
    },
    {
      key: "project3",
      name: "React",
      img: react,
      desc: "During my studies I had many opurtunities to make some tasks and small projects with React technology. I have basic and fundamental understand of that how React work, what makes me belive that it will make easier for me in the future to jump in to higher levels.",
    },
    {
      key: "project4",
      name: "Angular",
      img: angular,
      desc: "I had a few opportunities to work with AngularJS - most of the time I was working on HTML templates, from time to time I had opportunity to write some functions in Angular structure.",
    },
    {
      key: "project5",
      name: "Wordpress & Joomla",
      img: wordpress,
      desc: "I realized one commercial project that was organized around Wordpress functionalities and ready plugins. Back in the Technical School I had occasion to create business site using CMS called Joomla.",
    },
  ]);

  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Typography
        variant="h4"
        component="h2"
        gutterBottom
        style={{
          textUnderlineOffset: "8px",
          marginTop: "30px",
        }}
      >
        Projects and experience
      </Typography>
      {projects.map((ele) => (
        <Elements data={ele} key={ele.key} />
      ))}
    </Box>
  );
}

function Elements(props) {
  const classes = useStyles();
  const [data] = React.useState(props["data"]);

  //intersection observer API implementation (use react-intersection-observer next time for cleaner code)
  const containerRef = React.useRef(null);

  const [isVisible, setIsVisible] = React.useState(false);
  const callbackFunction = (entries) => {
    const [entry] = entries;
    setIsVisible(entry.isIntersecting);
  };

  React.useEffect(() => {
    let observerRefValue = null;

    const options = {
      root: null,
      rootMargin: "0px",
      threshold: 0.2,
    };

    const observer = new IntersectionObserver(callbackFunction, options);
    if (containerRef.current) {
      observer.observe(containerRef.current);
      observerRefValue = containerRef.current;
    }
    return () => {
      if (observerRefValue) observer.unobserve(observerRefValue);
    };
  }, [containerRef]);

  return (
    <Card
      style={{ maxWidth: "800px", marginTop: "30px" }}
      className={isVisible ? classes.visible : classes.noVisible}
      ref={containerRef}
    >
      <CardActionArea>
        <CardMedia className={classes.media} image={data.img} title="Project" />
        <CardContent className={classes.content}>
          <Typography gutterBottom variant="h5" component="h2">
            {data.name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {data.desc}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
