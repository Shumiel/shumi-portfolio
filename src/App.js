//General imports
import header_bg from "./img/header_bg.png";
import "./App.css";
import { useEffect, useState } from "react";
import { makeStyles, createStyles } from "@material-ui/core/styles";
//Custome React Components
import About from "./components/About.js";
import Projects from "./components/Projects.js";
import Additional from "./components/Additional.js";
//Material UI Components - START
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import CopyrightIcon from '@material-ui/icons/Copyright';
//Material UI Components - END

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      ...theme.typography,
      padding: theme.spacing(1),
      fontSize: theme.spacing(2),
      backgroundColor: "#132A13",
    },
    additional: {  
      ...theme.box,   
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    Home:{
      zIndex: '100'
    }
  })
);

function App() {
  const [example, setExample] = useState("Pancake");
  const classes = useStyles();

  useEffect(() => {
    //call only once when mounted
    setTimeout(() => setExample("Marcin"), 500);
  }, []);

  return (
    <div className="App">
      {/** Home */}
      <Box className={classes.Home}>
        <img
          className="App-header-bg"
          src={header_bg}
          alt="App-header-bg"
        ></img>
        <Box className="App-header"></Box>{" "}
        <Box className="App-header-text">
          <Container className="pulsate">
            <Container>Hi, I'm {example}.</Container>
            <Container>Welcome to my Portfolio</Container>
          </Container>
        </Box>
      </Box>

      {/** About */}     
      <About />
      

      {/** Projects */}
      <Projects />


      {/** Additional info */}
      <Box className={classes.additional}>
        <Additional />
      </Box>

      {/** Footer */}
      <Box className="5">
        <Container style={{
          backgroundColor: '#5b6681',
          width:'100%', margin:'0 auto',padding:'0 auto'
        }}><Typography>Copyright 2021 <CopyrightIcon  style={{transform: 'translateY(4px)'}} fontSize={"small"}/> </Typography> </Container>
      </Box>
    </div>
  );
}

export default App;
